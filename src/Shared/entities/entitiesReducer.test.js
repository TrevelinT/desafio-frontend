import reducer, { getAllComicBooks } from './entitiesReducer'
import { FETCH_COMICS_SUCCESS } from '../../ComicBookList/ComicBookActions';

test('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual({
        comicBooks: {}
    });
});

test('should handle FETCH_COMICS_SUCCESS', () => {
    const comicBooks = {
        '1': {
            id: 1
        }
    };
    expect(reducer(undefined, {
        type: FETCH_COMICS_SUCCESS,
        payload: {
            comicBooks,
        }
    })).toEqual({
        comicBooks
    });
});

test('should handle getAllComics selector', () => {
    const comicBooks = {
        '1': {
            id: 1
        }
    };
    expect(getAllComicBooks({
        entities: {
            comicBooks
        }
    })).toEqual(comicBooks);
});