import merge from 'lodash.merge';
import { FETCH_COMICS_SUCCESS } from "../../ComicBookList/ComicBookActions";
// ComicBooks state shape
// comicBooks: {
//     '1': {
//         id: '1',
//         name: '',
//         date: '',
//         description: '',
//         edition: '',
//     }
// }

const initialState = {
    comicBooks: {}
}

// This reducer only merge the comicBook entities
export default function entitiesReducer(previousState = initialState, action) {
    switch(action.type) {
        case FETCH_COMICS_SUCCESS:
            return {
                ...previousState,
                comicBooks: merge(
                    previousState.comicBooks,
                    action.payload.comicBooks
                )
            };
        default:
            return previousState;
    }
}

// Selectors
export function getAllComicBooks(state) {
    return state.entities.comicBooks;
}