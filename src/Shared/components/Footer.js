import React from 'react';

export default function Footer() {
    return (
        <footer className="footer">
            <small>2017 Danilo Trevelin</small>
            <small><a href="https://github.com/TrevelinT/">Github</a></small>
        </footer>
    )
}
