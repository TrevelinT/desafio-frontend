import React from 'react';
import { Link } from 'react-router-dom';

export default function Header() {
    return (
        <div>
            <header className="app-header">
                <h1 className="app-header-logo"><img src="https://i.annihil.us/u/prod/misc/marvel.svg" className="app-header-logo-image" alt="Marvel logo" /></h1>
                <p className="app-header-nav"><Link to="/">Front-end challenge</Link></p>
            </header>
            <nav className="app-nav">
                <h2 className="app-nav-section"><Link to="/">Front-end challenge</Link></h2>
                <ul className="app-nav-list">
                    <li className="app-nav-list-item"><Link to="/">Disclaimer</Link></li>
                    <li className="app-nav-list-item"><Link to="/">Comics</Link></li>
                </ul>
            </nav>
        </div>
    )
}
