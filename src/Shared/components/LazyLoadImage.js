import React, { Component } from 'react';

class LazyLoadImage extends Component {
    constructor(props) {
        super(props);

        this.state = { isImageLoaded: false };
        this.handleImageLoad = this.handleImageLoad.bind(this);
    }

    handleImageLoad() {
        this.setState(prevState => ({
            isImageLoaded: !prevState.isImageLoaded
        }));
    }

    render() {
        const {
            image,
            onLoad,
            className,
            ...otherProps,
        } = this.props
        const loadingStyles = {
            width: '100%',
            height: '0',
            paddingTop: `${(1 / this.props.proportion) * 100}%`,
            background: '#5e5e5e',
            position: 'relative'
        };
        const loadingClassName = this.state.isImageLoaded ? null : 'is-loading-image';

        return (
            <div style={loadingStyles}>
                <img
                    src={image}
                    className={[loadingClassName, 'lazy-load-image', className].join(' ').trim()}
                    onLoad={this.handleImageLoad}
                    {...otherProps}
                />
            </div>
        )
    }
}

export default LazyLoadImage;

