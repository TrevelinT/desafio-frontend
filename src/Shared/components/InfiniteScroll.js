import React, { Component } from 'react';
import PropTypes from 'prop-types';
import throttle from 'lodash.throttle';

const propTypes = {
    isLoadingItems: PropTypes.bool.isRequired,
    loadMoreItems: PropTypes.func.isRequired,
};

class InfiniteScroll extends Component {
    constructor(props) {
        super(props);

        this.handleScroll = throttle(ev => this.checkIfNeedsMoreContent(ev), 1000)
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll, false);
    }
    
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll, false);
    }

    checkIfNeedsMoreContent(ev) {
        // To understand this concept, check the link:
        // https://css-tricks.com/debouncing-throttling-explained-examples/
        const distanceFromBottomOfPage = ev.srcElement.body.scrollHeight - window.scrollY - window.innerHeight;
        // This distance is equal 75% of the document. Because the scroll height increases, we need to think
        // the inverse, so we pass 25% of the document
        const distanceToFireEvent = ev.srcElement.body.scrollHeight / 4;
        if (distanceFromBottomOfPage < distanceToFireEvent) {
            if (!this.props.isLoadingItems) {
                this.props.loadMoreItems();
            }
        }
    }

    render() {
        return (
            <div className="infinite-scroll">
                {this.props.children}
            </div>
        )
    }
}

InfiniteScroll.propTypes = propTypes;

export default InfiniteScroll;