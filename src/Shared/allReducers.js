import { combineReducers } from 'redux';
import comicBooks from '../ComicBookList/comicBookReducers'
import entities from './entities/entitiesReducer'

export default combineReducers({
    comicBooks,
    entities,
})