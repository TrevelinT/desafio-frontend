import React from 'react';
import { Link } from 'react-router-dom';
import Header from '../Shared/components/Header';
import Footer from '../Shared/components/Footer';

export default function Disclaimer() {
    return (
        <div>
            <Header />
            <article className="disclaimer">
                <section className="box">
                <div className="box-inner">
                    <p>This is the front-end challenge. The layout is heavely inspired in actual Marvel site</p>
                    <Link className="button-primary" to="/comic-book">See test</Link>
                </div>
                </section>
            </article>
            <Footer />
        </div>
    )
}