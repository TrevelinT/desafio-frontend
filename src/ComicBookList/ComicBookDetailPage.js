import React, { Component } from 'react';
import { connect } from 'react-redux';
import ComicBookDetail from './ComicBookDetail';
import { getAllComicBooks } from '../Shared/entities/entitiesReducer';
import { getComicBook } from './comicBookReducers';
import { fetchComics } from './ComicBookActions';
import { Link } from 'react-router-dom';
import Header from '../Shared/components/Header';
import Footer from '../Shared/components/Footer';

class ComicBookDetailPage extends Component {
    componentDidMount() {
        this.props.fetchComics('Iron man');
    }

    render() {
        return (
            <div>
                <Header />
                <nav className="submenu">
                    <Link className="submenu-item" to="/comic-book">Back to series</Link>
                </nav>
                <ComicBookDetail {...this.props.comicBook}/>
                <Footer />
            </div>
        )
    }
}

export { ComicBookDetailPage };

function mapStateToProps(state, ownProps) {
    const allComics = getAllComicBooks(state);

    return {
        comicBook: getComicBook(allComics, ownProps.match.params.id)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchComics: (character) => dispatch(fetchComics(character))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ComicBookDetailPage);