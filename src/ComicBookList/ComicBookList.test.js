import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import { ComicBookList } from './ComicBookList';

test('Render component', () => {
    const comics = [
        {
            id: 36420,
            title: 'Iron Man (2013) #258.3',
            date: '2013-05-22T00:00:00-0400',
            description: 'Micheline. Layton. Two voices that defined Iron Man come together to tell the untold story of his most dire hour &ndash; Armor Wars 2!',
            writer: 'David Michelinie',
            artist: 'Bob Layton',
            image: 'http://i.annihil.us/u/prod/marvel/i/mg/d/d0/5189540981b44.jpg',
        }
    ];
    const component = renderer.create(
        <MemoryRouter>
            <ComicBookList
                comics={comics}
            />
        </MemoryRouter>
    );
    let tree = component.toJSON();
    
    expect(tree).toMatchSnapshot();
});