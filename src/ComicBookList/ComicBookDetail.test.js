import React from 'react';
import renderer from 'react-test-renderer';
import ComicBookDetail from './ComicBookDetail';

test('Render component', () => {
    const component = renderer.create(
        <ComicBookDetail
            title={'Iron Man #10'}
            image={'test'}
            date={'2013-11-06T00:00:00-0500'}
            description={'blablablabla'}
            writer={'Writer'}
            artist={'Artist'}
        />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
})