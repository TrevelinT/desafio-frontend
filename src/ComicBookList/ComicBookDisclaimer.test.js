import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import ComicBookDisclaimer from './ComicBookDisclaimer';

test('Render component', () => {
    const component = renderer.create(
        <MemoryRouter>
            <ComicBookDisclaimer />
        </MemoryRouter>
    );
    let tree = component.toJSON();

    expect(tree).toMatchSnapshot();
});
