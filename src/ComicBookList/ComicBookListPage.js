import React, { Component } from 'react'
import { connect } from 'react-redux';
import { fetchComicsIfNeeded, fetchMoreComicsIfNeeded } from './ComicBookActions';
import { getAllComicBooks } from '../Shared/entities/entitiesReducer';
import { getComicBookList, getComicBook } from './comicBookReducers';
import { ComicBookList } from './ComicBookList';
import InfiniteScroll from '../Shared/components/InfiniteScroll';
import Header from '../Shared/components/Header';
import Footer from '../Shared/components/Footer';

class ComicBookListPage extends Component {
    componentDidMount() {
        this.props.fetchComicsIfNeeded('Iron man');
    }

    render() {
        return (
            <div>
                <Header />
                <header>
                    <h1 className="title">List of comics featuring Iron Man</h1>
                </header>
                {this.props.comicBooks ? <InfiniteScroll isLoadingItems={this.props.isFetching} loadMoreItems={() => this.props.fetchMoreComicsIfNeeded('Iron man')}><ComicBookList comics={this.props.comicBooks} /></InfiniteScroll> : 'loading...'}
                <Footer />
            </div>
        )
    }
}

export { ComicBookListPage };

function mapStateToProps(state) {
    const allComics = getAllComicBooks(state)
    return {
        comicBooks: getComicBookList(state).map(comicBook => getComicBook(allComics, comicBook)),
        isFetching: state.comicBooks.isFetching
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchComicsIfNeeded: (character) => dispatch(fetchComicsIfNeeded(character)),
        fetchMoreComicsIfNeeded: (character) => dispatch(fetchMoreComicsIfNeeded(character)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ComicBookListPage);
