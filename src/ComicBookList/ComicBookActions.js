import $ from 'jquery';
import mock from '../Shared/mock.json';

export const FETCH_COMICS_REQUEST = 'FETCH_COMICS_REQUEST'
export const FETCH_COMICS_FAILURE = 'FETCH_COMICS_FAILURE'
export const FETCH_COMICS_SUCCESS = 'FETCH_COMICS_SUCCESS'
export const SELECT_COMIC = 'SELECT_COMIC'

export function requestComics(character) {
    return {
        type: FETCH_COMICS_REQUEST,
        payload: { character }
    }
}

export function fetchComicsFailure(error) {
    return {
        type: FETCH_COMICS_FAILURE,
        payload: error,
        error: true,
    }
}

function normalizeComicBookResponse(response) {
    const normalizedResults = response.results.reduce((comics, comic) => {
        const { id, thumbnail, creators } = comic;

        return {
            comicBooks: {
                ...comics.comicBooks,
                [id]: {
                    id: id,
                    title: comic.title,
                    date: (comic.dates.find(date => date.type === 'onsaleDate')  || {}).date,
                    description: comic.description && decodeURIComponent(
                        comic.description
                            .replace(/\<([^>])*\>/g, '')
                            .replace(/\n/g, '')
                    ),
                    writer: (creators.items.find(creator => creator.role === 'writer') ||{}).name,
                    artist: (creators.items.find(creator => creator.role === 'artist') ||{}).name,
                    image: `${thumbnail.path}.${thumbnail.extension}`,
                }
            },
            result: (comics.result || []).concat(id)
        }
    }, {})

    return {
        ...normalizedResults,
        total: response.total,
    }
}

export function fetchComicsSuccess(response) {
    return {
        type: FETCH_COMICS_SUCCESS,
        payload: response,
    }
}

export function selectComic(comicBook) {
    return {
        type: SELECT_COMIC,
        payload: { comicBook },
    }
}

export function fetchComicsIfNeeded(character) {
    return (dispatch, getState) => {
        const comicBooks = getState().comicBooks
        if (!comicBooks.items.length) {
            return dispatch(fetchComics(character))
        }
    }
}

export function fetchMoreComicsIfNeeded(character) {
    return (dispatch, getState) => {
        const comicBooks = getState().comicBooks
        if (comicBooks.offset < comicBooks.total) {
            return dispatch(fetchComics(character))
        }
    }
}

// Thunk action
export function fetchComics(character) {
    return (dispatch, getState) => {
        dispatch(requestComics(character))
        $
            .get('https://gateway.marvel.com:443/v1/public/characters/1009368/comics?orderBy=-onsaleDate&offset=' + getState().comicBooks.offset + '&apikey=670c47d4888e493334435e0880c1b690')
            .done(response => {
                dispatch(fetchComicsSuccess(normalizeComicBookResponse(response.data)))
            })
            .fail(error => {
                dispatch(fetchComicsFailure(error))
            })
    } 
}