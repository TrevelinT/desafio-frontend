import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Link } from 'react-router-dom';
import LazyLoadImage from '../Shared/components/LazyLoadImage';

const propTypes = {
    comics: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        date: PropTypes.string,
        description: PropTypes.string,
    })).isRequired
};

function ComicBookList(props) {
    function renderComicBookItem(comicBook) {
        return (
            <li className="comicbook-item" key={comicBook.id}>
                <Link to={`/comic-book/${comicBook.id}`}><LazyLoadImage proportion={0.66} image={comicBook.image} alt={comicBook.title} /></Link>
                <h1 className="comicbook-item-title">{comicBook.title}</h1>
                <p className="comicbook-item-data">{moment(comicBook.date).format('MMMM D, YYYY')}</p>
                <p className="comicbook-item-description">{comicBook.description}</p>
            </li>
        );
    }

    return (
        <ul className="comicbook-list">
            {props.comics.map(renderComicBookItem)}
        </ul>
    );
}

ComicBookList.propTypes = propTypes

export { ComicBookList };
