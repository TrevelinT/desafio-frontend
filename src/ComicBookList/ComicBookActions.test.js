import { requestComics, FETCH_COMICS_REQUEST, FETCH_COMICS_FAILURE, fetchComicsFailure, SELECT_COMIC, selectComic, FETCH_COMICS_SUCCESS, fetchComicsSuccess } from "./ComicBookActions";

test('should create an action to fetch comics', () => {
    const character = 'Iron man';
    const expectedAction = {
        type: FETCH_COMICS_REQUEST,
        payload: {
            character
        }
    };
    expect(requestComics(character)).toEqual(expectedAction);
})

test('should create an action to show that fetch comics failed', () => {
    const error = new Error();
    const expectedAction = {
        type: FETCH_COMICS_FAILURE,
        payload: error,
        error: true,
    };
    expect(fetchComicsFailure(error)).toEqual(expectedAction);
})

test('should create an action to select comic book', () => {
    const comicBook = 2312324;
    const expectedAction = {
        type: SELECT_COMIC,
        payload: {
            comicBook
        }
    };
    expect(selectComic(comicBook)).toEqual(expectedAction);
})

test('should create an action to fetch comics successfully', () => {
    const response = {
        comicBooks: {},
        result: [],
        total: 0,
    };
    const expectedAction = {
        type: FETCH_COMICS_SUCCESS,
        payload: response,
    };
    expect(fetchComicsSuccess(response)).toEqual(expectedAction);
})