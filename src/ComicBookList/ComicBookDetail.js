import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const propTypes = {
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    writer: PropTypes.string.isRequired,
    artist: PropTypes.string.isRequired,
    description: PropTypes.string,
};

const defaultProps = {
    description: 'No description'
};
    
function ComicBookDetail(props) {
    return (
        <article className="comicbook-detail">
            <header>
                <h1 className="comicbook-detail-title">{props.title}</h1>
            </header>
            <section className="media-object">
                <div className="media-image">
                    <img src={props.image}/>
                </div>
                <div className="media-content">
                    <p className="comicbook-detail-data"><strong>Published:</strong> {moment(props.date).format('MMMM D, YYYY')}</p>
                    <p className="comicbook-detail-data"><strong>Writer:</strong> {props.writer}</p>
                    <p className="comicbook-detail-data"><strong>Penciller:</strong> {props.artist}</p>
                    <p className="comicbook-detail-description">{props.description}</p>
                </div>
            </section>
        </article>
    );
}

ComicBookDetail.propTypes = propTypes;
ComicBookDetail.defaultProps = defaultProps;

export default ComicBookDetail;