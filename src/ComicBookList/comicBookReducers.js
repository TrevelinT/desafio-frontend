import union from 'lodash.union';
import {
    FETCH_COMICS_FAILURE,
    FETCH_COMICS_REQUEST,
    FETCH_COMICS_SUCCESS,
    SELECT_COMIC
} from "./ComicBookActions";

const initialState = {
    current: '',
    isFetching: false,
    total: 0,
    offset: 0,
    error: null,
    items: [],
}

export default function comicBookReducer(previousState = initialState, action) {
    switch (action.type) {
        case FETCH_COMICS_REQUEST:
            return {
                ...previousState,
                isFetching: true,
                error: null,
            };
        case FETCH_COMICS_SUCCESS:
            return {
                ...previousState,
                isFetching: false,
                offset: previousState.offset + 20,
                total: action.payload.total,
                items: union(previousState.items, action.payload.result)
            };
        case FETCH_COMICS_FAILURE:
            return {
                ...previousState,
                isFetching: false,
                error: action.payload.error
            };
        case SELECT_COMIC:
            return {
                ...previousState,
                current: action.payload.comicBook
            };
        default:
            return previousState;
    }
}

export function getComicBookList(state) {
    return state.comicBooks.items;
}

export function getComicBook(allComicBooks, id) {
    return allComicBooks[id];
}