import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import comicBookApp from './Shared/allReducers';
import { AppContainer } from 'react-hot-loader';

let store = createStore(
    comicBookApp,
    compose(
        applyMiddleware(thunk),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);

const render = Component => {
    ReactDOM.render(
        <AppContainer>
        <Provider store={store}>
                <Component />
        </Provider>
        </AppContainer>,
        document.getElementById('root')
    );
}

render(App);

if (module.hot) {
    module.hot.accept('./App', () => {
        const NextApp = require('./App').default;
        render(NextApp)
    });
}
