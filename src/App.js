import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import ComicBookListPage from './ComicBookList/ComicBookListPage';
import ComicBookDetailPage from './ComicBookList/ComicBookDetailPage';
import ComicBookDisclaimer from './ComicBookList/ComicBookDisclaimer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route path="/comic-book/:id" component={ComicBookDetailPage} />
            <Route path="/comic-book" component={ComicBookListPage} />
            <Route path="/" component={ComicBookDisclaimer} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;